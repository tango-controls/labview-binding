@ECHO off

::------------------------------------------------------------------------
:: ======== TANGO BINDING PATHS ======== 
::------------------------------------------------------------------------
::- git clone directory
set BINDING_PATH=C:\Projects\labview-binding@gitlab
::- runtime directory
set RUNTIME_PATH_X86=C:\Projects\tango-10.0.0-rc2-v143-x86
set RUNTIME_PATH_X64=C:\Projects\tango-10.0.0-rc2-v143-x64
::- .h
set RUNTIME_INC_X86=%RUNTIME_PATH_X86%\include
set RUNTIME_INC_X64=%RUNTIME_PATH_X64%\include
::- .lib
set RUNTIME_LIB_X86=%RUNTIME_PATH_X86%\lib
set RUNTIME_LIB_X64=%RUNTIME_PATH_X64%\lib
::- .dll
set RUNTIME_BIN_X86=%RUNTIME_PATH_X86%\bin
set RUNTIME_BIN_X64=%RUNTIME_PATH_X64%\bin
::- path
set PATH=%RUNTIME_BIN_X86%\bin;%PATH%
set PATH=%RUNTIME_BIN_X64%\bin;%PATH%
set PATH=%BINDING_PATH%\runtime;%PATH%

::------------------------------------------------------------------------
:: ======== NI-LABVIEW PATHS ======== 
::------------------------------------------------------------------------
set LV_ROOT32=%BINDING_PATH%\labview\2020
set LV_INC32=%LV_ROOT32%\cintools-x86
set LV_LIB32=%LV_ROOT32%\cintools-x86
SET LV_LIB32_LIST=labview.lib

set LV_ROOT64=%BINDING_PATH%\labview\2024
set LV_INC64=%LV_ROOT64%\cintools-x64
set LV_LIB64=%LV_ROOT64%\cintools-x64
SET LV_LIB64_LIST=labview.lib

::------------------------------------------------------------------------
:: ======== ZMQ ======== 
::------------------------------------------------------------------------
SET ZMQ_ROOT32=%RUNTIME_PATH_X86%
SET ZMQ_INC32=%RUNTIME_INC_X86%
SET ZMQ_LIB32=%RUNTIME_LIB_X86%
SET ZMQ_BIN32=%RUNTIME_BIN_X86%
SET ZMQ_LIB32_LIST=libzmq-v141-mt-4_0_5.lib
SET PATH=%ZMQ_BIN32%;%PATH%

SET ZMQ_ROOT64=%RUNTIME_PATH_X64%
SET ZMQ_INC64=%RUNTIME_INC_X64%
SET ZMQ_LIB64=%RUNTIME_LIB_X64%
SET ZMQ_BIN64=%RUNTIME_BIN_X64%
SET ZMQ_LIB64_LIST=libzmq-v141-mt-4_0_5.lib
SET PATH=%ZMQ_BIN64%;%PATH%

::------------------------------------------------------------------------
:: ======== OMNIORB ======== 
::------------------------------------------------------------------------
SET OMNIORB_ROOT32=%RUNTIME_PATH_X86%
SET OMNIORB_INC32=%RUNTIME_INC_X86%
SET OMNIORB_LIB32=%RUNTIME_LIB_X86%
SET OMNIORB_BIN32=%RUNTIME_BIN_X86%
SET OMNIORB_LIB32_LIST=omnithread_rt.lib;omniORB4_rt.lib;COS4_rt.lib;omniDynamic4_rt.lib
SET PATH=%OMNIORB_BIN32%;%PATH%

SET OMNIORB_ROOT64=%RUNTIME_PATH_X64%
SET OMNIORB_INC64=%RUNTIME_INC_X64%
SET OMNIORB_LIB64=%RUNTIME_LIB_X64%
SET OMNIORB_BIN64=%RUNTIME_BIN_X64%
SET OMNIORB_LIB64_LIST=omnithread_rt.lib;omniORB4_rt.lib;COS4_rt.lib;omniDynamic4_rt.lib
SET PATH=%OMNIORB_BIN64%;%PATH%

::------------------------------------------------------------------------
:: ======== TANGO ======== 
::------------------------------------------------------------------------
SET TANGO_ROOT32=%RUNTIME_PATH_X86%
SET TANGO_INC32=%RUNTIME_INC_X86%\tango
SET TANGO_LIB32=%RUNTIME_LIB_X86%
SET TANGO_BIN32=%RUNTIME_BIN_X86%
SET TANGO_LIB32_LIST=turbojpeg.lib;tango.lib
SET PATH=%TANGO_BIN32%;%PATH%

SET TANGO_ROOT64=%RUNTIME_PATH_X64%
SET TANGO_INC64=%RUNTIME_INC_X64%%\tango
SET TANGO_LIB64=%RUNTIME_LIB_X64%
SET TANGO_BIN64=%RUNTIME_BIN_X64%
SET TANGO_LIB64_LIST=turbojpeg.lib;tango.lib
SET PATH=%TANGO_BIN64%;%PATH%