//----------------------------------------------------------------------------
// Copyright (c) 2004-2021 Synchrotron SOLEIL
// All rights reserved. This program and the accompanying materials
// are made available under the terms of the GNU Lesser Public License v3
// which accompanies this distribution, and is available at
// http://www.gnu.org/licenses/lgpl.html
//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
// YAT LIBRARY
//----------------------------------------------------------------------------
//
// Copyright (C) 2006-2021  The Tango Community
//
// Part of the code comes from the ACE Framework (i386 asm bytes swaping code)
// see http://www.cs.wustl.edu/~schmidt/ACE.html for more about ACE
//
// The thread native implementation has been initially inspired by omniThread
// - the threading support library that comes with omniORB.
// see http://omniorb.sourceforge.net/ for more about omniORB.
//
// Contributors form the TANGO community:
// Ramon Sune (ALBA) for the Signal class
//
// The YAT library is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation; either version 2 of the License, or (at your option)
// any later version.
//
// The YAT library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
// Public License for more details.
//
// See COPYING file for license details
//
// Contact:
//      Stephane Poirier
//      Synchrotron SOLEIL
//------------------------------------------------------------------------------
/*!
 * \author S.Poirier - Synchrotron SOLEIL
 */

//=============================================================================
// DEPENDENCIES
//=============================================================================
#include <cstring>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "yat/file/FileName.h"
#include "yat/time/Time.h"
#include "yat/time/Timer.h"

namespace yat
{

  ///===========================================================================
  /// FileName
  ///===========================================================================
  size_t FileName::s_copy_bloc_size = 1048576;

  //----------------------------------------------------------------------------
  // FileName::access_from_string
  //----------------------------------------------------------------------------
  mode_t FileName::access_from_string(const std::string &strAccess)
  {
    mode_t mode = 0;
    sscanf(PSZ(strAccess), "%o", &mode);
    return mode;
  }

  //-------------------------------------------------------------------
  // FileName::path_exist()
  //-------------------------------------------------------------------
  bool FileName::path_exist() const
  {
    if (is_null())
      return true;

    pcsz pszPath = PSZ(full_name());
    if (strchr(pszPath, '*') || strchr(pszPath, '?'))
      // there are wildcard. this is not a valid path
      return false;

    uint32 uiLen = strlen(pszPath);
    if (uiLen == 0)
      return false;

    struct stat st;
    int iRc;

    if (uiLen >= 2 && IsSepPath(pszPath[uiLen - 1]) &&
        pszPath[uiLen - 2] != ':')
    {
      // Path ends with '\' => remove '\'
      std::string strPath = pszPath;
      strPath = strPath.substr(0, strPath.size() - 1);
      iRc = stat(PSZ(strPath), &st);
      if (iRc && errno != ENOENT)
        ThrowExceptionFromErrno(StringFormat(ERR_STAT_FAILED).format(strPath),
                                "FileName::path_exist");
    }
    else
    {
      iRc = stat(pszPath, &st);
      if (iRc && errno != ENOENT)
        ThrowExceptionFromErrno(StringFormat(ERR_STAT_FAILED).format(pszPath),
                                "FileName::path_exist");
    }
    return !iRc && (st.st_mode & S_IFDIR);
  }

  //-------------------------------------------------------------------
  // FileName::file_exist
  //-------------------------------------------------------------------
  bool FileName::file_exist() const
  {
    if (is_null())
      return true;

    pcsz pcszfull_name = full_name().c_str();

    struct stat st;
    return (!access(pcszfull_name, F_OK) && !stat(pcszfull_name, &st) &&
            (st.st_mode & S_IFREG));
  }

  //-------------------------------------------------------------------
  // FileName::file_access
  //-------------------------------------------------------------------
  bool FileName::file_access() const
  {
    if (is_null())
      return true;

    return !access(PSZ(full_name()), F_OK);
  }

  //----------------------------------------------------------------------------
  // FileName::set_full_name
  //----------------------------------------------------------------------------
  void FileName::set_full_name(pcsz pszFileName)
  {
    if (!pszFileName || !pszFileName[0])
    {
      m_strFile = StringUtil::empty;
      return;
    }

    std::string strFileName = pszFileName;

    // Convert separators
    convert_separators(&strFileName);

    if (strFileName == null_path || strFileName == null_file_name ||
        yat::String(strFileName).match("/dev/null/*"))
    {
      m_strFile = null_path;
      return;
    }

    if (IsSepPath(strFileName[0u]))
    {
      // Absolute name
      m_strFile = strFileName;
    }
    else
    {
      // relative name: add current working directory
      char cbuf[_MAX_PATH];
      char *unused = getcwd(cbuf, _MAX_PATH);
      m_strFile =
          StringFormat("{}/{}").format(yat::String(cbuf)).format(strFileName);
    }
  }

  //-------------------------------------------------------------------
  // FileName::rel_name
  //-------------------------------------------------------------------
  std::string FileName::rel_name(const char *pszPath) const
  {
    if (is_null())
      return "";

    FileName fnRef(pszPath);

    // Search for first separator. If not => return full name
    const char *p = strchr(m_strFile.c_str(), SEP_PATH);
    const char *pRef = strchr(fnRef.full_name().c_str(), SEP_PATH);
    if (!p || !pRef)
      return m_strFile;

    std::string str;
    bool bClimbStarted = false;
    for (;;)
    {
      const char *p1 = strchr(p + 1, SEP_PATH);
      const char *pRef1 = strchr(pRef + 1, SEP_PATH);

      if (!p1)
      {
        // No more parts in file name
        while (pRef1)
        {
          str = std::string("../") + str;
          pRef1 = strchr(pRef1 + 1, SEP_PATH);
        }
        str += std::string(p + 1);
        return str;
      }

      if (!pRef1)
      {
        // No more reference
        str += std::string(p + 1);
        return str;
      }

      // Compare directories
      if ((p1 - p != pRef1 - pRef) || bClimbStarted ||
          // Unix : le case est important
          strncmp(p, pRef, p1 - p))
      {
        // Different directory
        str = std::string("../") + str;
        bClimbStarted = true;
        str.append(p + 1, p1 - p);
      }
      p = p1;
      pRef = pRef1;
    }
  }

  //----------------------------------------------------------------------------
  // FileName::convert_separators
  //----------------------------------------------------------------------------
  void FileName::convert_separators(std::string *pstr)
  {
    char *ptc = new char[pstr->length() + 1];
    char *pStart = ptc;
    strcpy(ptc, PSZ(*pstr));

    // Convert from DOS to UNIX
    while (*ptc)
    {
      if (*ptc == SEP_PATHDOS)
        *ptc = SEP_PATHUNIX;
      ptc++;
    }

    *pstr = pStart;
    delete[] pStart;
  }

  //----------------------------------------------------------------------------
  // FileName::mkdir
  //----------------------------------------------------------------------------
  void FileName::mkdir(mode_t mode, uid_t uid, gid_t gid) const

  {
    if (is_null())
      // cannot create the null file
      return;

    std::string str = path();
    if (str.empty())
      return;

    char *p;
    p = ::strchr(const_cast<char *>(str.c_str()), SEP_PATH);

    if (!p)
    {
      std::string strErr = StringFormat(ERR_CANNOT_CREATE_FOLDER).format(str);
      throw BadPathException(PSZ(strErr), "FileName::mkdir");
    }
    p = strchr(p + 1, SEP_PATH);
    if (!p)
    {
      // path = racine ; exist
      return;
    }

    do
    {
      *p = 0;
      struct stat st;
      if (::stat(PSZ(str), &st))
      {
        if (errno != ENOENT)
          // stat call report error != file not found
          ThrowExceptionFromErrno(StringFormat(ERR_STAT_FAILED).format(str),
                                  "FileName::mkdir");

        if (::mkdir(PSZ(str), 0000777))
        {
          // failure
          ThrowExceptionFromErrno(
              StringFormat(ERR_CANNOT_CREATE_FOLDER).format(str),
              "FileName::mkdir");
        }

        // Change access mode if needed
        if (mode != 0)
        {
          if (::chmod(PSZ(str), mode))
          {
            // changing access mode failed
            ThrowExceptionFromErrno(
                StringFormat(ERR_CHMOD_FAILED).format(str).format(mode),
                "FileName::mkdir");
          }
        }
        // Change owner if needed
        if ((int)uid != -1 || (int)gid != -1)
        {
          if (::chown(PSZ(str), uid, gid))
          {
            // changing owner mode failed
            ThrowExceptionFromErrno(StringFormat(ERR_CHOWN_FAILED)
                                        .format(str)
                                        .format(uid)
                                        .format(gid),
                                    "FileName::mkdir");
          }
        }
      }
      else
      {
        if (!(st.st_mode & S_IFDIR))
        {
          // c'est un fichier : erreur
          std::string strErr = StringFormat(ERR_CANNOT_CREATE_FOLDER).format(str);
          throw BadPathException(strErr, "FileName::mkdir");
        }
        // Directory : ok
      }
      // Next path component
      *p = SEP_PATH;
      p = strchr(p + 1, SEP_PATH);
    } while (p);
  }

  //----------------------------------------------------------------------------
  // FileName::link_exist
  //----------------------------------------------------------------------------
  bool FileName::link_exist() const
  {
    if (is_null())
      // The null file is not a link
      return false;

    struct stat st;
    std::string strFullName = full_name();
    if (is_path_name())
      strFullName.erase(strFullName.size() - 1, 1);
    int iRc = lstat(PSZ(strFullName), &st);
    if (!iRc && S_ISLNK(st.st_mode))
      return true;

    /* Probably stupid... no file => no link!
      else if( iRc )
      {
        std::string strErr = StringUtil::str_format(ERR_TEST_LINK,
      PSZ(full_name())); ThrowExceptionFromErrno(PSZ(strErr),
      "FileName::link_exist");
      }
    */

    return false;
  }

  //----------------------------------------------------------------------------
  // FileName::make_sym_link
  //----------------------------------------------------------------------------
  void FileName::make_sym_link(const std::string &strTarget, uid_t uid,
                               gid_t gid) const
  {
    if (is_null())
      return;

    int iRc = symlink(PSZ(strTarget), PSZ(full_name()));
    if (iRc)
    {
      std::string strErr = StringFormat(ERR_CANNOT_CREATE_LINK)
                               .format(full_name())
                               .format(strTarget);
      ThrowExceptionFromErrno(PSZ(strErr), "FileName::make_sym_link");
    }
    // Change owner if needed
    if ((int)uid != -1 || (int)gid != -1)
    {
      if (lchown(PSZ(full_name()), uid, gid))
      {
        // changing owner mode failed
        std::string strErr = StringFormat(ERR_CHOWN_FAILED)
                                 .format(full_name())
                                 .format(uid)
                                 .format(gid);
        ThrowExceptionFromErrno(strErr, "FileName::make_sym_link");
      }
    }
  }

  //----------------------------------------------------------------------------
  // FileName::size
  //----------------------------------------------------------------------------
  uint32 FileName::size() const
  {
    if (is_null())
      return 0;

    struct stat sStat;
    if (stat(PSZ(full_name()), &sStat) == -1)
    {
      std::string strErr = StringFormat(ERR_CANNOT_FETCH_INFO).format(m_strFile);
      ThrowExceptionFromErrno(PSZ(strErr), "FileName::size");
    }
    return sStat.st_size;
  }

  //----------------------------------------------------------------------------
  // FileName::size64
  //----------------------------------------------------------------------------
  uint64 FileName::size64() const
  {
    if (is_null())
      return 0;

    struct stat64 sStat;
    if (stat64(PSZ(full_name()), &sStat) == -1)
    {
      std::string strErr = StringFormat(ERR_CANNOT_FETCH_INFO).format(m_strFile);
      ThrowExceptionFromErrno(PSZ(strErr), "FileName::size");
    }
    return sStat.st_size;
  }

  //----------------------------------------------------------------------------
  // FileName::mod_time
  //----------------------------------------------------------------------------
  void FileName::mod_time(Time *pTm, bool bLocalTime, bool stat_link) const
  {
    if (is_null())
    {
      pTm->set_long_unix(0);
      return;
    }

    struct stat sStat;
    int rc = 0;
    if (stat_link)
      rc = lstat(PSZ(full_name()), &sStat);
    else
      rc = stat(PSZ(full_name()), &sStat);
    if (rc < 0)
    {
      std::string strErr =
          StringFormat(ERR_CANNOT_GET_FILE_TIME).format(m_strFile);
      ThrowExceptionFromErrno(strErr, "FileName::mod_time");
    }

    if (bLocalTime)
    {
      struct tm tmLocal;
      localtime_r(&sStat.st_mtime, &tmLocal);
      pTm->set_long_unix(mktime(&tmLocal) + tmLocal.tm_gmtoff);
    }
    else
      pTm->set_long_unix(sStat.st_mtime);
  }

  //----------------------------------------------------------------------------
  // FileName::set_mod_time
  //----------------------------------------------------------------------------
  void FileName::set_mod_time(const Time &tm) const
  {
    if (is_null())
      return;

    struct utimbuf sTm;
    struct stat sStat;

    if (stat(PSZ(full_name()), &sStat) != -1)
      // Get access time, in order to preserve it
      sTm.actime = sStat.st_atime;
    else
      // stat function failed, use the new mod time
      sTm.actime = tm.long_unix();

    sTm.modtime = tm.long_unix();
    if (utime(PSZ(full_name()), &sTm))
    {
      std::string strErr =
          StringFormat(ERR_CANNOT_CHANGE_FILE_TIME).format(m_strFile);
      ThrowExceptionFromErrno(PSZ(strErr), "FileName::set_mod_time");
    }
  }

  //-------------------------------------------------------------------
  // FileName::priv_copy
  //-------------------------------------------------------------------
  void FileName::priv_copy(const std::string &strDst, yat::String *md5sum_p,
                           bool keep_metadata)
  {
  }

  //-------------------------------------------------------------------
  // FileName::move
  //-------------------------------------------------------------------
  void FileName::move(const std::string &strDest)
  {
    if (!file_exist())
    { // File doesn't exists
      std::string strErr = StringFormat(ERR_FILE_NOT_FOUND).format(m_strFile);
      throw FileNotFoundException(strErr, "FileName::move");
    }

    FileName fDst(strDest);
    if (!fDst.is_null() && fDst.is_path_name())
      // Take source name
      fDst.set(fDst.path(), name_ext());

    // Remove destination
    if (!fDst.is_null() && fDst.file_exist())
      fDst.remove();

    // Check filesystem id, if it's the same, we can try to rename file
    fsid_t idSrc = file_system_id();
    fsid_t idDst = fDst.file_system_id();
    if (!is_null() && !fDst.is_null() && idSrc.__val[0] == idDst.__val[0] &&
        idSrc.__val[1] == idDst.__val[1])
    {
      try
      {
        rename(fDst.full_name());
      }
      catch (Exception e)
      {
        // Unable to rename => make a copy
        copy(fDst.full_name(), true);

        // Deletes source file and changes name
        remove();
        set(fDst.full_name());
      }
    }
    else
    {
      copy(fDst.full_name(), true);

      // Deletes source file and changes name
      remove();
      set(fDst.full_name());
    }
  }

  //-------------------------------------------------------------------
  // FileName::file_system_type
  //-------------------------------------------------------------------
  FileName::FSType FileName::file_system_type() const
  {
    struct statfs buf;
    int iRc = statfs(PSZ(path()), &buf);
    if (iRc)
    {
      std::string strErr = StringFormat(ERR_FSTYPE).format(path());
      ThrowExceptionFromErrno(PSZ(strErr), "FileName::file_system_type");
    }
    return FSType(buf.f_type);
  }

  //-------------------------------------------------------------------
  // FileName::file_system_id
  //-------------------------------------------------------------------
  fsid_t FileName::file_system_id() const
  {
    struct statfs buf;

    if (is_null())
    {
      ::memset(&buf, 0, sizeof(struct statfs));
      return buf.f_fsid;
    }

    int iRc = statfs(PSZ(path()), &buf);
    if (iRc)
    {
      std::string strErr = StringFormat(ERR_FSTYPE).format(path());
      ThrowExceptionFromErrno(PSZ(strErr), "FileName::file_system_type");
    }
    return buf.f_fsid;
  }

  //-------------------------------------------------------------------
  // FileName::file_system_statistics
  //-------------------------------------------------------------------
  FileName::FSStat FileName::file_system_statistics() const
  {
    if (is_null())
    {
      return FSStat();
    }

    struct statvfs buf;

    int iRc = statvfs(PSZ(path()), &buf);
    if (iRc)
    {
      std::string strErr = StringFormat(ERR_FSTYPE).format(path());
      ThrowExceptionFromErrno(strErr, "FileName::file_system_type");
    }

    FSStat stats;
    stats.size.bytes = (yat::uint64)buf.f_blocks * (yat::uint64)buf.f_frsize;
    stats.avail.bytes = (yat::uint64)buf.f_bavail * (yat::uint64)buf.f_frsize;
    stats.used.bytes = stats.size - stats.avail;
    stats.avail_percent = 100.0 * double(stats.avail) / double(stats.size);
    stats.used_percent = 100.0 - stats.avail_percent;

    return stats;
  }

  //-------------------------------------------------------------------
  // FileName::info
  //-------------------------------------------------------------------
  void FileName::info(Info *info_p, bool follow_link) const
  {
    if (is_null())
    {
      info_p->clear();
      return;
    }

    struct stat64 st;
    int rc = 0;

    if (follow_link)
      rc = stat64(PSZ(full_name()), &st);
    else
      rc = lstat64(PSZ(full_name()), &st);
    if (rc)
    {
      info_p->is_exist = false;
    }
    else
    {
      info_p->is_exist = true;
      info_p->size = st.st_size;
      info_p->mode = st.st_mode;
      info_p->is_file = (st.st_mode & S_IFREG) == S_IFREG;
      info_p->is_dir = (st.st_mode & S_IFDIR) == S_IFDIR;
      info_p->is_link = (st.st_mode & S_IFLNK) == S_IFLNK;
      info_p->uid = st.st_uid;
      info_p->gid = st.st_gid;
      mod_time(&(info_p->mod_time), true, !follow_link);
    }
  }

  //-------------------------------------------------------------------
  // FileName::chmod
  //-------------------------------------------------------------------
  void FileName::chmod(mode_t mode)
  {
    if (is_null())
      return;

    int iRc = ::chmod(PSZ(full_name()), mode);
    if (iRc)
    {
      std::string strErr =
          StringFormat(ERR_CHMOD_FAILED).format(full_name()).format(mode);
      ThrowExceptionFromErrno(PSZ(strErr), "FileName::chmod");
    }
  }

  //-------------------------------------------------------------------
  // FileName::chown
  //-------------------------------------------------------------------
  void FileName::chown(uid_t uid, gid_t gid)
  {
    if (is_null())
      return;

    int iRc = ::chown(PSZ(full_name()), uid, gid);
    if (iRc)
    {
      std::string strErr = StringFormat(ERR_CHOWN_FAILED)
                               .format(full_name())
                               .format(uid)
                               .format(gid);
      ThrowExceptionFromErrno(PSZ(strErr), "FileName::chown");
    }
  }

  //-------------------------------------------------------------------
  // FileName::ThrowExceptionFromErrno
  //-------------------------------------------------------------------
  void FileName::ThrowExceptionFromErrno(const String &desc,
                                         const String &origin)
  {
    std::string strDesc = StringFormat("{}. [Errno {}] {}")
                              .format(desc)
                              .format(int(errno))
                              .format(static_cast<const char *>(strerror(errno)));
    switch (errno)
    {
    case EIO:
    case EDEADLK:
    case EFAULT:
    case EINTR:
    case EINVAL:
    case ENOLCK:
      throw IOException(strDesc, origin);
    case EPERM:
    case EACCES:
    case EROFS:
      throw PermissionException(strDesc, origin);
    case ENOTEMPTY:
      throw BadPathConditionException(strDesc, origin);
    case ENAMETOOLONG:
    case ELOOP:
    case EISDIR:
    case ENOTDIR:
    case EBADF:
      throw BadPathException(strDesc, origin);
    case ENOENT:
      throw FileNotFoundException(strDesc, origin);
    default:
      throw Exception(String("FILE_ERROR"), strDesc, origin);
    }
  }

  //-------------------------------------------------------------------
  // FileName::set_copy_bloc_size
  //-------------------------------------------------------------------
  void FileName::set_copy_bloc_size(size_t size) { s_copy_bloc_size = size; }

  //-------------------------------------------------------------------
  // FileName::md5sum
  //-------------------------------------------------------------------
  yat::String FileName::md5sum() const
  {
    return "";
  }

  //===========================================================================
  // Class FileEnum
  //===========================================================================

  //-------------------------------------------------------------------
  // Initialisation
  //-------------------------------------------------------------------
  FileEnum::FileEnum(const std::string &strPath, EEnumMode eMode)
  {
    m_dirDir = NULL;
    init(strPath, eMode);
  }

  //-------------------------------------------------------------------
  // Destructeur
  //-------------------------------------------------------------------
  FileEnum::~FileEnum() { close(); }

  //-------------------------------------------------------------------
  // FileEnum::init
  //-------------------------------------------------------------------
  void FileEnum::init(const std::string &strPath, EEnumMode eMode)
  {
    close();
    m_eMode = eMode;
    set(PSZ(strPath));

    // Initialize enumeration
    m_dirDir = opendir(path().c_str());
    if (NULL == m_dirDir)
    {
      std::string strErr = StringFormat(ERR_CANNOT_ENUM_DIR).format(path());
      throw BadPathException(PSZ(strErr), "FileEnum::Init");
    }

    m_strPath = strPath; // Save initial path.

    // translate win separator to unix superator
    StringUtil::replace(&m_strPath, SEP_PATHDOS, SEP_PATHUNIX);
  }

  //-------------------------------------------------------------------
  // FileEnum::find
  //-------------------------------------------------------------------
  bool FileEnum::find()
  {
    struct dirent *dirEntry;
    std::string str;
    while ((dirEntry = readdir(m_dirDir)) != NULL)
    {
      str = dirEntry->d_name;
      if (StringUtil::is_equal(str, ".") == false &&
          StringUtil::is_equal(str, "..") == false)
      {
        // Set new file name
        set(m_strPath, dirEntry->d_name);

        // Check file
        if ((m_eMode & ENUM_FILE) && file_exist())
          return true;
        if ((m_eMode & ENUM_DIR) && path_exist())
          return true;
      }
    }

    // Not found
    return false;
  }

  //-------------------------------------------------------------------
  // FileEnum::close
  //-------------------------------------------------------------------
  void FileEnum::close()
  {
    if (m_dirDir)
      closedir(m_dirDir);
  }
} // namespace yat
