// ============================================================================
//
// = CONTEXT
//   This is part of the Tango binding for LabVIEW
//
// = FILENAME
//   Config.h
//
// = AUTHOR
//   Nicolas Leclercq - Synchrotron SOLEIL - France
//
// ============================================================================

#pragma once

#if _WIN32 || _WIN64
  #pragma warning(disable : 4251)
  #if _WIN64
    #define _TBFL_64_
  #else
    #define _TBFL_32_
  #endif
#endif

#if __GNUC__
  #if __x86_64__ || __ppc64__
    #define _TBFL_64_
  #else
    #define _TBFL_32_
  #endif
#endif

//- console messages & traces
#define _TBFL_VERBOSE_

//- device-server support
#define _TBFL_HAS_DEVICE_SERVER_SUPPORT_
