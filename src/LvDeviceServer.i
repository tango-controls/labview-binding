// ============================================================================
//
// = CONTEXT
//   This is part of the Tango binding for LabVIEW
//
// = FILENAME
//   DeviceManager.i
//
// = AUTHOR
//   Nicolas Leclercq - Synchrotron SOLEIL - France
//
// ============================================================================

//=============================================================================
// LvDeviceServer::instance
//=============================================================================
LV_INLINE LvDeviceServer* LvDeviceServer::instance ()
{
  if ( ! LvDeviceServer::instance_ )
  {
    Tango::Except::throw_exception(_CPTC_("unexpected null pointer"),
                                   _CPTC_("the Tango device-server instance is not [or not properly] initialized"),
                                   _CPTC_("LvDeviceServer::instance"));
  }

  return LvDeviceServer::instance_;
}

